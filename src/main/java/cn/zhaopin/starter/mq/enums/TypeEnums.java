package cn.zhaopin.starter.mq.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description: MQ类型枚举
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/09-15:22
 */
@Getter
@AllArgsConstructor
public enum TypeEnums {

    /** pulsar */
    PULSAR("pulsar"),
    ;

    private String type;
}
