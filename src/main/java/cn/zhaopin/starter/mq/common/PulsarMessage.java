package cn.zhaopin.starter.mq.common;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Map;

/**
 * Description: 自定义 message
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date 2021/07/31 13:28
 */
public class PulsarMessage implements Serializable {

    private Object payload;

    private Map<String, Object> headers;

    public PulsarMessage() {
    }

    protected PulsarMessage(@NotNull Object payload, @Nullable Map<String, Object> headers) {
        this.payload = payload;
        this.headers = headers;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(@NotNull Object payload) {
        this.payload = payload;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(@Nullable Map<String, Object> headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "PulsarMessage{" +
                "payload=" + payload +
                ", headers=" + headers +
                '}';
    }
}
