package cn.zhaopin.starter.mq.common;

import org.apache.pulsar.client.api.MessageId;

import java.util.Map;

/**
 * Description: 消息补充对象
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/22-16:14
 */
public class PulsarMessageExt<T> {

    private String topic;

    private MessageId messageId;

    private T body;

    private Map<String, Object> headers;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public MessageId getMessageId() {
        return messageId;
    }

    public void setMessageId(MessageId messageId) {
        this.messageId = messageId;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "PulsarMessageExt{" +
                "topic='" + topic + '\'' +
                ", messageId=" + messageId +
                ", body=" + body +
                ", headers=" + headers +
                '}';
    }

}
