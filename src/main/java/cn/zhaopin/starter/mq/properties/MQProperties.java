package cn.zhaopin.starter.mq.properties;

import cn.zhaopin.starter.mq.constant.MQConstant;
import cn.zhaopin.starter.mq.enums.TypeEnums;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: mq配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/09-15:20
 */
@Data
@ConfigurationProperties(prefix = MQConstant.PREFIX)
public class MQProperties {

    private String type = TypeEnums.PULSAR.getType();

    private PulsarProperties pulsar;

}
