package cn.zhaopin.starter.mq.properties;

import lombok.Data;

/**
 * Description: pulsar 配置信息
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/09-15:28
 */
@Data
public class PulsarProperties {

    /** 服务url  */
    private String serviceUrl;

    /** 认证token */
    private String authenticationToken;

    /** 集群id : pulsar-*** */
    private String clusterId;

    /** 命名空间 */
    private String namespace;

}
