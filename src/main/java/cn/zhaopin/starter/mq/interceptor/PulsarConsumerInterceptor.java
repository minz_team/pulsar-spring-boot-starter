package cn.zhaopin.starter.mq.interceptor;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.ConsumerInterceptor;
import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageId;

import java.util.Set;

/**
 * Description: 自定义 consumer拦截器
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/08/03-14:09
 */
public class PulsarConsumerInterceptor implements ConsumerInterceptor {

    @Override
    public void close() {

    }

    @Override
    public Message beforeConsume(Consumer consumer, Message message) {
        return null;
    }

    @Override
    public void onAcknowledge(Consumer consumer, MessageId messageId, Throwable throwable) {

    }

    @Override
    public void onAcknowledgeCumulative(Consumer consumer, MessageId messageId, Throwable throwable) {

    }

    @Override
    public void onAckTimeoutSend(Consumer consumer, Set set) {

    }

    @Override
    public void onNegativeAcksSend(Consumer consumer, Set set) {

    }
}
