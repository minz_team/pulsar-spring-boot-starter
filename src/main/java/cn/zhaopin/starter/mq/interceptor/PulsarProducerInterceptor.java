package cn.zhaopin.starter.mq.interceptor;


import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageId;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.interceptor.ProducerInterceptor;

/**
 * Description: 自定义producer拦截器
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/08/03-14:18
 */
public class PulsarProducerInterceptor implements ProducerInterceptor {

    @Override
    public void close() {

    }

    @Override
    public boolean eligible(Message message) {
        return false;
    }

    @Override
    public Message beforeSend(Producer producer, Message message) {
        return null;
    }

    @Override
    public void onSendAcknowledgement(Producer producer, Message message, MessageId messageId, Throwable throwable) {

    }
}
