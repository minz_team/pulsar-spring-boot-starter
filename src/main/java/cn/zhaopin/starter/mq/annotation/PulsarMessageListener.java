package cn.zhaopin.starter.mq.annotation;

import org.apache.pulsar.client.api.SubscriptionInitialPosition;
import org.apache.pulsar.client.api.SubscriptionType;

import java.lang.annotation.*;

/**
 * Description: 消息监听
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/20-10:04
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PulsarMessageListener {

    /** topic 简称 系统自动拼接 */
    String topic() default "";


    /** 订阅名称 */
    String subscriptionName() default "";

    /**
     * <p>订阅模式</p>
     * <p>1 传统的发布-订阅消息 :  每个消费者都有一个唯一的订阅名称（独占）</p>
     * <p>2 传统的消息队列消息 :  多个消费者使用同一个的订阅名称（共享、灾备）</p>
     * <p>3 同时实现以上两点，可以让一些消费者使用独占方式，剩余消费者使用其他方式。</p>
     * <p>仅共享消费模式支持重试和死信</p>
     *
     */
    SubscriptionType subscriptionType() default SubscriptionType.Shared;

    /**
     * <p>订阅开始消费位置</p>
     * <p>Latest : 从最近的位置开始消费, 历史的数据会导致消费不到</p>
     * <p>Earliest : 从最早的位置开始消费, 历史数据每次都会被消费,建议做好幂等处理</p>
     *
     */
    SubscriptionInitialPosition subscriptionInitialPosition() default SubscriptionInitialPosition.Latest;

}
