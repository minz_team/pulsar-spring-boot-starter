package cn.zhaopin.starter.mq.core;

import cn.zhaopin.starter.mq.properties.PulsarProperties;

/**
 * topic factory
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/21-15:13
 */
public class PulsarTopicFactory {

    /** topic格式为 persistent://集群（租户）ID/命名空间/Topic名称 */
    private static final String TOPIC_PATTERN = "persistent://%s/%s/%s";

    private PulsarProperties pulsarProperties;

    public PulsarTopicFactory(PulsarProperties pulsarProperties) {
        this.pulsarProperties = pulsarProperties;
    }

    public void setTdmqProperties(PulsarProperties pulsarProperties) {
        this.pulsarProperties = pulsarProperties;
    }

    public  String obtainFullTopic(String topic) {
        return String.format(TOPIC_PATTERN, pulsarProperties.getClusterId(), pulsarProperties.getNamespace(), topic);
    }
}
