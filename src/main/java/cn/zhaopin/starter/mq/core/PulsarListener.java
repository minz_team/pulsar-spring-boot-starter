package cn.zhaopin.starter.mq.core;

import cn.zhaopin.starter.mq.common.PulsarMessageExt;

/**
 * 监听消费者
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/20-10:20
 */
public interface PulsarListener<T> {

    /**
     * <p>pulsar consumer listener message</p>
     * return true : Confirm message reached
     * return false : Confirm message not reached
     * throws exception : Confirm message not reached
     *
     * @param message message
     * @return ack flag
     * @throws Exception exp
     */
    boolean onMessage(PulsarMessageExt<T> message) throws Exception;
}
