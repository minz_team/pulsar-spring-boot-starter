package cn.zhaopin.starter.mq.config;

import cn.zhaopin.starter.mq.properties.MQProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Description: MQ 配置
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/06/30-10:31
 */
@Configuration
@EnableConfigurationProperties(value = {MQProperties.class})
public class MQConfiguration {

}
