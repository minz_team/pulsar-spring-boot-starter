package cn.zhaopin.starter.mq.support;

/**
 * Description: id 生成器
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/22-15:40
 */
@FunctionalInterface
public interface IdGenerator {

    String generate();
}
