package cn.zhaopin.starter.mq.support;

import org.springframework.util.AlternativeJdkIdGenerator;

/**
 * Description: UUID 生成器
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/22-15:43
 */
public class UUIDGenerator implements IdGenerator{

    private static final org.springframework.util.IdGenerator defaultIdGenerator = new AlternativeJdkIdGenerator();

    @Override
    public String generate() {
        return defaultIdGenerator.generateId().toString();
    }
}
