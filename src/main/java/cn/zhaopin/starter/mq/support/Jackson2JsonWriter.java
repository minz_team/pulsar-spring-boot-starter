package cn.zhaopin.starter.mq.support;

import cn.zhaopin.starter.mq.common.PulsarMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.pulsar.client.api.SchemaSerializationException;
import org.apache.pulsar.client.api.schema.SchemaWriter;

/**
 * Description: 自定义json writer
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/22-10:17
 */
public class Jackson2JsonWriter implements SchemaWriter<PulsarMessage> {

    private final ObjectMapper objectMapper;

    Jackson2JsonWriter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public byte[] write(PulsarMessage message) {
        try {
            return this.objectMapper.writeValueAsBytes(message);
        } catch (JsonProcessingException jsonProcessingException) {
            throw new SchemaSerializationException(jsonProcessingException);
        }
    }
}
