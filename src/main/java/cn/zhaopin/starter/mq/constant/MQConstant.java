package cn.zhaopin.starter.mq.constant;

/**
 * Description: 常量
 *
 * @author: zuomin (myleszelic@outlook.com)
 * @date: 2021/07/12-09:12
 */
public final class MQConstant {

    public static final String PREFIX = "app.mq";

    public static class MqType {
        public static final String PULSAR = "pulsar";
        public static final String ROCKET_MQ = "rocketmq";
    }

    public static class Header {
        public static final String ID = "id";
        public static final String TIMESTAMP = "timestamp";
    }
}
